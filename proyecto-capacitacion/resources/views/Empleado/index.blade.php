<!--Extendiende de los layouts.blade.php-->
@extends('layouts.layout')
<!--Nos indica a continuacion el contenido de la pantalla-->
@section('content')
<div class="row">
    <section class="content">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="pull-left">
                        <h3>Lista Empleados</h3>
                    </div>
                    <div class="col-xs-12 col-md-12">
                        <a href="{{ route('empleado.create') }}" class="btn btn-info">Agregar</a>
                    </div>
                    <div class="table-container">
                        <table id="mytable" class="table table-bordred table-striped">
                            <thead>
                                <th>Nombre</th>
                                <th>Puesto</th>
                                <th>Email</th>
                                <th>Edad</th>
                                <th>Antigüedad</th>
                                <th>Sueldo</th>
                                <th>Moneda</th>
                                <th>Ver datos</th>
                                <th>Editar</th>
                                <th>Eliminar</th>
                            </thead>
                            <tbody>
                                <!--Condicion para ver si la lista viene vacia -->
                                @if($empleados->count())
                                <!--Iteramos la lista de empleado para llenar cada uno de los campos de la tabla-->
                                @foreach($empleados as $empleado)
                                <tr>
                                    <td>{{$empleado->nombre}}</td>
                                    <td>{{$empleado->puesto}}</td>
                                    <td>{{$empleado->email}}</td>
                                    <td>{{$empleado->edad}}</td>
                                    <td>{{$empleado->antiguedad}}</td>
                                    <td>{{$empleado->sueldo}}</td>
                                    <td>{{$empleado->moneda_sueldo}}</td>
                                    <!--Aqui obtiene el id del empleado seleccionado y se lo manda a controller-->
                                    <td><a class="btn btn-default btn-xs" href="{{action('EmpleadoController@show', $empleado->id)}}">Ver</a></td>
                                    <td><a class="btn btn-primary btn-xs" href="{{action('EmpleadoController@edit', $empleado->id)}}">Editar</a></td>
                        
                                    <td><a class="btn btn-danger btn-xs" data-toggle="modal" href="#deleteProperty{{$empleado->id}}">Eliminar</a></td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="8">No hay registro !!</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Modal -->
<div class="modal fade" id="deleteProperty{{$empleado->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-center" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <strong>"¿Esta seguro que desa Eliminar el registro?
                    <br>Recuerde que la acción es ireversible"</strong>
            </div>
            <div class="modal-footer">
                <form action="{{action('EmpleadoController@destroy', $empleado->id)}}" method="post">
                                            {{csrf_field()}}
                                            <input name="_method" type="hidden" value="DELETE">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button class="btn btn-danger" type="submit">Eliminar</button>
                </form> 
            </div>
        </div>
    </div>
</div>
@endsection
</div>