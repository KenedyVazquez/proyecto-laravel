<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    //EL NOMBRE DE LA TABLA DE LA BASE DATOS A CUAL APUNTA EN ESTE CASO EMPLEADO
    protected $table = 'empleado';
    //CAMPOS DE LA TABLA DE LA TABLA QUE VAMOS A MANIPULAR
    protected $fillable = ['nombre','puesto', 'email','edad','antiguedad','sueldo','moneda_sueldo'];
}
