<?php

namespace App\Http\Controllers;

use App\Empleado;
use Illuminate\Http\Request;

class EmpleadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //AQUI TRAEMOS LOS EMPLEDOS DE LA BASE DE DATOS Y SE LO AGREGAMOS A LA VARIABLE $EMPLEADOS
        $empleados = Empleado::orderBy('id', 'DESC')->paginate(3);
        //AQUI RETORNAMOS UNA VISTA EN LA CARPETA EMPLEADO EN EL ARCHIVO INDEX.BLADE.PHP 
        //LE INYECTAMOS A LA VISTA LA VARIABLE EMPLEADOS QUE VIENE DE LA CONSULTA
        return view('empleado.index', compact('empleados'));
    }
    public function create()
    {
        //LLAMAMOS AL METODO GETWSOAP QUE RETORNA LOS DATOS DE SOAP  Y SE LO ASIGNAMOS AL CURRENCIES
        $currencies = $this->getWSSoap();
        //OBTENEMOS EL STRING DE CON LOS DATOS DE MXN
        $monedas = $currencies->AllCurrenciesResult;
        //CONVERTINMOS EL STRING EN UN ARRAY Y SE LO ASIGNAMOS A LA VARIABLE $ARRAYMONEDAS
        $arraymonedas = explode(";", $monedas);
        return view('empleado.create', compact('arraymonedas'));
    }
    public function show($id)
    {
        //$id ES EL VALOR ENVIADO DESDE LA VISTA INDEX
        //AQUI TRAEMOS AL EMPLEDO DE LA BASE DE DATOS Y SE LO AGREGAMOS A LA VARIABLE $EMPLEADO
        $empleado = Empleado::find($id);
        //AQUI RETORNAMOS UNA VISTA EN LA CARPETA EMPLEADO EN EL ARCHIVO SHOW.BLADE.PHP 
        //LE INYECTAMOS A LA VISTA LA VARIABLE EMPLEADO QUE VIENE DE LA CONSULTA
        return view('empleado.show', compact('empleado'));
    }
    public function destroy($id)
    {
        //$id ES EL VALOR ENVIADO DESDE LA VISTA INDEX
        //AQUI VERIFICAMOS SI EL EMPLEDO DE LA BASE DE DATOS FUE ELIMINADO 
        Empleado::find($id)->delete();
        //AQUI RETORNAMOS UNA VISTA EN LA CARPETA EMPLEADO EN EL ARCHIVO INDEX.BLADE.PHP
        return redirect()->route('empleado.index')->with('success', 'Registro eliminado');
    }

    public function edit($id)
    {
        //$id ES EL VALOR ENVIADO DESDE LA VISTA INDEX
        //AQUI TRAEMOS AL EMPLEDO DE LA BASE DE DATOS Y SE LO AGREGAMOS A LA VARIABLE $EMPLEADO
        $empleado = Empleado::find($id);
        //LLAMAMOS AL METODO GETWSOAP QUE RETORNA LOS DATOS DE SOAP  Y SE LO ASIGNAMOS AL CURRENCIES
        $currencies = $this->getWSSoap();
        //OBTENEMOS EL STRING DE CON LOS DATOS DE MXN
        $monedas = $currencies->AllCurrenciesResult;
        //CONVERTINMOS EL STRING EN UN ARRAY Y SE LO ASIGNAMOS A LA VARIABLE $ARRAYMONEDAS
        $arraymonedas = explode(";", $monedas);
        //AQUI RETORNAMOS UNA VISTA EN LA CARPETA EMPLEADO EN EL ARCHIVO EDITH.BLADE.PHP 
        //LE INYECTAMOS A LA VISTA LA VARIABLE EMPLEADO QUE VIENE DE LA CONSULTA Y EL ARRAY DE DE TIPO DE MONEDAS
        return view('empleado.edit', compact('empleado', 'arraymonedas'));
    }
    public function store(Request $request)
    {
        //$request ES EL OBJETO DE EMPLEADO  ENVIADO DESDE LA VISTA FORMULARIO  CREATE.BLADE.PHP
        //AQUI VALIDAMOS CADA UNO DE LOS CAMPOS DE FORMULARIO 
        $this->validate($request, [
            'nombre' => 'required|max:50',
            'puesto'    => 'required|between:3,25',
            'email'     => 'required|email',
            'edad'      => 'required|numeric',
            'antiguedad'    => 'required|numeric',
            'sueldo'        => 'required|numeric',
            'moneda_sueldo' => 'required'
        ]);
      //AQUI SE GUARDO EL EMPLEDO DE LA BASE DE DATOS
        Empleado::create($request->all());
         //AQUI RETORNAMOS UNA VISTA EN LA CARPETA EMPLEADO EN EL ARCHIVO INDEX.BLADE.PHP
        return redirect()->route('empleado.index')->with('success', 'Registro Creado');
    }
    public function update(Request $request, $id)
    {
        //$id ES EL VALOR ENVIADO DESDE LA VISTA INDEX INDEX.BLADE.PHP
        //$request ES EL OBJETO DE EMPLEADO  ENVIADO DESDE LA VISTA FORMULARIO  EDIT.BLADE.PHP
        //AQUI VALIDAMOS CADA UNO DE LOS CAMPOS DE FORMULARIO 
        $this->validate(
            $request,
            [
                'nombre' => 'required|max:50',
                'puesto'    => 'required|between:3,25',
                'email'     => 'required|email',
                'edad'      => 'required|numeric',
                'antiguedad'    => 'required|numeric',
                'sueldo'        => 'required|numeric',
                'moneda_sueldo' => 'required'
            ]
        );
        //AQUI TRAEMOS AL EMPLEDO DE LA BASE DE DATOS EL QUE SE ACTUALIZO EN LA VARIABLE $EMPLEADO
        Empleado::find($id)->update($request->all());
        //AQUI RETORNAMOS UNA VISTA EN LA CARPETA EMPLEADO EN EL ARCHIVO INDEX.BLADE.PHP
        return redirect()->route('empleado.index')->with('success', 'Registro Actualizado');
    }

    private function getWSSoap()
    {
        //DE LA API SOAP OBTENER LAS INFORMAICION Y LO GUARDAMOS EN UNA VARIABLE WSDL
        $wsdl = "https://fx.currencysystem.com/webservices/CurrencyServer5.asmx?wsdl";
        //CREA UNA INSTANCIA  DE SOAPCLIENTE  
        $client = new \SoapClient($wsdl, ["licenceKey" => "", 'trace' => true]);
        //OBTENEMOS LA INFORMACION DE CLIENTE 
        $resultClient = $client->AllCurrencies();
        return $resultClient;
    }
}
