<?php

namespace App\Helpers;

class ComunHelper{


     /**
     * @param $mensaje
     * @return bool|string
     * @author Omar
     */
    public static function set_message_bot_telegram(){
        $mensaje = 'hola desde aqui';
        $apiToken = env('1659845460:AAG7QxXIMyfW1fLqzZIObRC1s4O7vyUBj_I');

        $data = [
            'chat_id' => '@pruebas_laravel',
            'text' => $mensaje
        ];

        return ComunHelper::getSslPage("https://api.telegram.org/bot$apiToken/sendMessage?" . http_build_query($data) );
    }

     /**
     * @param $url
     * @return bool|string
     * @author Omar
     */
    public static function getSslPage($url){
        $ch=curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_REFERER, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;

    }

}