<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', 'HomeController@index')->name('home');
//RUTA PARA IR AL CONTROLLER EmpleadoControlle AL METODO INDEX
Route::get('empleados','EmpleadoController@index')->name('empleado.index');
//RUTA PARA IR AL CONTROLLER EmpleadoControlle AL METODO DESTROY
Route::delete('empleado/{empleado}','EmpleadoController@destroy');
//RUTA PARA IR AL CONTROLLER EmpleadoControlle AL METODO EDIT
Route::get('empleado/{empleado}/edit','EmpleadoController@edit')->name('empleado.edit');
//RUTA PARA IR AL CONTROLLER EmpleadoControlle AL METODO CREATE
Route::get('empleado/create', 'EmpleadoController@create')->name('empleado.create');
//RUTA PARA IR AL CONTROLLER EmpleadoControlle AL METODO STORE
Route::post('empleado', 'EmpleadoController@store')->name('empleado.store');
//RUTA PARA IR AL CONTROLLER EmpleadoControlle AL METODO SHOW
Route::get('empleado/{empleado}','EmpleadoController@show')->name('empleado.show');
//RUTA PARA IR AL CONTROLLER EmpleadoControlle AL METODO UPDATE
Route::put('empleado/{empleado}', 'EmpleadoController@update')->name('empleado.update');
Auth::routes();